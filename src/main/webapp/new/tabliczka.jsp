<%--
  Created by IntelliJ IDEA.
  User: Lukasz
  Date: 18.07.2020
  Time: 11:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table>
    <%
        String rozmiarX = request.getParameter("rozmiarX");
        String rozmiarY = request.getParameter("rozmiarY");
        int rozmiarXint ;
        int rozmiarYint ;
        try {
            rozmiarXint = Integer.parseInt(rozmiarX);
        } catch (NumberFormatException nef){
            rozmiarXint = 10;
        }

        try {
            rozmiarYint = Integer.parseInt(rozmiarY);
        } catch (NumberFormatException nef){
            rozmiarYint = 10;
        }

        for (int i =1;i<rozmiarXint;i++){
            out.print("<tr>");
            for (int j=1;j<rozmiarYint;j++){
                out.print("<td>");
                out.print(i*j);
                out.print("</td>");
            }
        }
    %>

    <FORM ACTION="new/tabliczka.jsp" METHOD="get">
        rozmiarX <input type="number" min="0" name="rozmiarX"/>
        rozmiarY <input type="number" min="0" name="rozmiarY"/>

        <input type="submit" name="query">
    </FORM>
</table>

</body>
</html>
