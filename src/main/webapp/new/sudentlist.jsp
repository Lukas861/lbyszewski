<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.sda34.webappdemo.model.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Lukasz
  Date: 18.07.2020
  Time: 13:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<table>
    <tr>
        <th>Index</th>
        <th>Fist Name</th>
        <th>Last Name</th>
        <th>Average</th>
        <th>Gender</th>
        <th>Is Active</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="student" items="${requestScope.studentList}">
        <tr>
        <td><c:out value="${student.indexNumber}"/></td>
        <td><c:out value="${student.firstName}"/></td>
        <td><c:out value="${student.lastNameName}"/></td>
        <td><c:out value="${student.average}"/></td>
        <td><c:out value="${student.gender}"/></td>
        <td><c:out value="${student.active}"/></td>
        <td>
            <a href="/students/edit?studentIndex=<c:out value="${student.indexNumber}"/>">Edit</a>
        </td>
        <td>
            <a href="/students/delete?studentIndex=<c:out value="${student.indexNumber}"/>">Delete</a>
        </td>
        </tr>
    </c:forEach>
</table>
<body>
<%



        %>
</body>
</html>
