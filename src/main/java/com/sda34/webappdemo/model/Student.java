package com.sda34.webappdemo.model;


import lombok.*;

@Getter
@Setter
@EqualsAndHashCode(of={"indexNumber"})
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private String indexNumber;
    private String firstName;
    private String lastName;
    private Double average;
    private Gender gender;
    private Boolean active;
}
