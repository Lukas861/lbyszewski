package com.sda34.webappdemo.servlets;

import com.sda34.webappdemo.model.Gender;
import com.sda34.webappdemo.model.Student;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/student")
public class StudentFormController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/new/student_form.jsp").forward(req,resp);

        String index = req.getParameter("index");
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String average = req.getParameter("average");
        String gender = req.getParameter("gender");
        String active = req.getParameter("active");


        Student student = Student.builder()
                .indexNumber(index)
                .firstName(firstName)
                .lastName(lastName)
                .average(Double.parseDouble(average))
                .active(Boolean.parseBoolean(active))
                .gender(Gender.valueOf(gender))
                .build();

        Object studentListResault = req.getSession().getAttribute("studentList");
        List<Student> studentList;
        if (studentListResault instanceof List){
            studentList = (List<Student>) studentListResault;
        }else  {
            studentList = new ArrayList<>();
        }


        studentList.add(student);
        req.getSession().setAttribute("student_list", studentList);
        resp.sendRedirect("/student");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
