package com.sda34.webappdemo.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("")
public class IndexController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String imie = req.getParameter("imie");
        if(imie != null && !imie.isEmpty()){
            req.setAttribute("imie_do_wyswietlenia", imie);
        }
        req.getRequestDispatcher("/index.jsp")
                .forward(req,resp);




        // super.doGet(req, resp);
    }
}
