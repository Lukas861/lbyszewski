<%@ page import="com.sda34.webappdemo.model.Student" %>
<%@ page import="com.sda34.webappdemo.model.Gender" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Lukasz
  Date: 18.07.2020
  Time: 13:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>studentSubmitHanded</title>
</head>
<body>


<%
    String index = request.getParameter("index");
    String firstName = request.getParameter("firstName");
    String lastName = request.getParameter("lastName");
    String average = request.getParameter("average");
    String gender = request.getParameter("gender");
    String active = request.getParameter("active");


    Student student = Student.builder()
            .indexNumber(index)
            .firstName(firstName)
            .lastName(lastName)
            .average(Double.parseDouble(average))
            .active(Boolean.parseBoolean(active))
            .gender(Gender.valueOf(gender))
            .build();





%>
</body>
</html>
